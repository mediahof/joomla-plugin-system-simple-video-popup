<?php
/**
 * @author     mediahof, Kiel-Germany
 * @link       http://www.mediahof.de
 * @copyright  Copyright (C) 2014 mediahof. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

class plgSystemSimple_Video_Popup extends JPlugin
{
    private $output = false;

    public function onAfterInitialise()
    {
        $app = JFactory::getApplication();

        if ($app->isAdmin() || !$this->params->get('video')) {
            return;
        }

        if ($this->params->get('menuitem') && $app->getMenu()->getActive()->id != $this->params->get('menuitem')) {
            return;
        }

        switch ($this->params->get('mode')) {
            case 'cookie':

                if ($app->input->cookie->get('simple_video_popup')) {
                    return;
                }

                $app->input->cookie->set('simple_video_popup', 1, time() + $this->params->get('lifetime', 720));
                break;

            case 'session':

                if ($app->getSession()->get('simple_video_popup')) {
                    return;
                }

                $app->getSession()->set('simple_video_popup', 1);

                break;
        }

        $this->output = true;
    }

    public function onBeforeRender()
    {

        if (!$this->output) {
            return;
        }

        $doc = JFactory::getDocument();

        $doc->addStyleDeclaration('#simple_video_popup{display:none;}');

        if (JVERSION >= 3) {
            JHtml::_('behavior.framework');
        }

        $js[] = 'jQuery(document).ready(function(){';
        $js[] = "window.setTimeout(function(){document.getElementById('simple_video_popup').click();}," . $this->params->get('delay', 250) . ');';
        $js[] = '});';

        $doc->addScriptDeclaration(implode($js));
    }

    public function onAfterRender()
    {
        if (!$this->output) {
            return;
        }

        switch ($this->params->get('plugin')) {
            default:
            case 'lightbox':
                $attributes['data-lightbox'] = 'width:' . $this->params->get('width', 640) . ';height:' . $this->params->get('height', 380) . ';';

                if ($this->params->get('title')) {
                    $attributes['title'] = $this->params->get('title');
                }
                break;

            case 'jcepopup':
                $attributes['class'] = 'jcepopup';
                $attributes['class'] = 'jcepopup';

                $attributes['rel'][] = 'width[' . $this->params->get('width', 640) . ']';
                $attributes['rel'][] = 'height[' . $this->params->get('height', 380) . ']';

                if ($this->params->get('title')) {
                    $attributes['rel'][] = 'title[' . $this->params->get('title') . ']';
                }

                $attributes['rel'] = implode('', $attributes['rel']);
                break;
        }

        $attributes['id'] = 'simple_video_popup';

        if ($this->params->get('autoplay')) {
            $this->params->set('video', $this->params->get('video') . '&amp;autoplay=1');
        }

        $video = JHtml::link($this->params->get('video'), '', $attributes) . PHP_EOL;

        $body = JResponse::getBody();

        $body = str_replace('</body>', $video . '</body>', $body);

        JResponse::setBody($body);
    }
}